import { Link } from "react-router-dom"; 
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import classes from '../styles.module.css';
import CardActions from '@material-ui/core/CardActions';
import Skeleton from '@material-ui/lab/Skeleton';

export default function PostLoad() {
	return (
		<div className={classes.postContainer}>

			<div className={classes.postContainerSkeleton}>
			<center>
			<h1> Please wait your post is loading. </h1>
				<Skeleton animation="wave">
					<Card className={classes.postCardSingle}> 
						<CardContent> 
						<Typography className={classes.title} color="textSecondary" gutterBottom>
				        </Typography>
				        <Typography variant="h5" component="h2">
				        </Typography>
				        <Typography className={classes.pos} color="textSecondary">
				        <div className={classes.tall}> words </div>
				        </Typography>
				        <Typography variant="body2" component="p">
				        </Typography>
					</CardContent>
					<CardActions>
			         <Link className={classes.link} to='/posts/'> <Button variant="outlined" color="primary" size="large">  Back </Button> </Link>
			      </CardActions>
					</Card>
				</Skeleton>
			</center>
			</div>
		</div>
	)
}