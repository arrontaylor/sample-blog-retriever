import React from "react";
import { Link } from "react-router-dom";
import { gql, useQuery } from "@apollo/client";
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import CardActions from '@material-ui/core/CardActions';
import Grid from '@material-ui/core/Grid';
import classes from '../styles.module.css';
import PostsLoad from './PostsLoad.js';

export const ALL_POSTS = gql`
  query {
	  posts {
	  	data {
			id
	      title
	      body
	    }
	  }
	}
`;

export default function Posts() {
  	const {loading, error, data} = useQuery(ALL_POSTS);
	if (loading) return <> <PostsLoad /> </>;
	if (error) return <p> error... </p>;

	return (
		<div className={classes.postContainer}>
			<Grid container>
			<h1> All Posts from: https://graphqlzero.almansi.me/api</h1>
				<Grid item xs={12}>
					<Grid container justify="center" spacing="5">
				
				{data.posts.data.map( (post) => { 
					let link = '/posts/'+post.id;
					return <Grid item> <Card key={post.id} className={classes.postCard}> 
								<CardContent> 
									<Typography className={classes.title} color="textSecondary" gutterBottom>
							          POST ID: {post.id}
							        </Typography>
							        <Typography variant="h5" component="h2">
							          {post.title} 
							        </Typography>
							        <Typography className={classes.pos} color="textSecondary">
							        </Typography>
							        <Typography variant="body2" component="p">
							          {post.body}
							        </Typography>

								</CardContent>
								<CardActions>
						         <Link className={classes.link} to={link}> <Button justify="right" variant="outlined" color="primary" size="medium">  Read Post {post.id} </Button> </Link>
						      </CardActions>
							</Card> </Grid>
					} )
				}
					</Grid>
				</Grid>
			</Grid>
		</div>
	)
}