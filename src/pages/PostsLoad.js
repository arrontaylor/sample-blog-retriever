import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import classes from '../styles.module.css';
import Grid from '@material-ui/core/Grid';
import Skeleton from '@material-ui/lab/Skeleton';

export default function PostsLoad() {
	return (
		<>
			<center> <h1> Please while wait your posts are loading... </h1> </center>

			<div className={classes.postContainerSkeleton} >
			<Grid container>
				<Grid item xs={12} >
					<Grid container justify="center" spacing="5" className={classes.postsLoad_Container}>
						<Grid item> 
							<Skeleton animation="wave">
								<Card className={classes.postCard}> 
									<CardContent> 
							       		<div className={classes.postsSkel}> words </div>
									</CardContent>
								</Card>
							</Skeleton>
						</Grid>
						<Grid item> 
							<Skeleton animation="wave">
								<Card className={classes.postCard}> 
									<CardContent> 
							       		<div className={classes.postsSkel}> words </div>
									</CardContent>
								</Card>
							</Skeleton>
						</Grid>
						<Grid item> 
							<Skeleton animation="wave">
								<Card className={classes.postCard}> 
									<CardContent> 
							       		<div className={classes.postsSkel}> words </div>
									</CardContent>
								</Card>
							</Skeleton>
						</Grid>	
						<Grid item> 
							<Skeleton animation="wave">
								<Card className={classes.postCard}> 
									<CardContent> 
							       		<div className={classes.postsSkel}> words </div>
									</CardContent>
								</Card>
							</Skeleton>
						</Grid>	
						<Grid item> 
							<Skeleton animation="wave">
								<Card className={classes.postCard}> 
									<CardContent> 
							       		<div className={classes.postsSkel}> words </div>
									</CardContent>
								</Card>
							</Skeleton>
						</Grid>	
					</Grid>
				</Grid>
			</Grid>
		</div>
		</>
	)
}