import { gql, useQuery } from "@apollo/client";
import  PostLoad from './PostLoad.js';
import { Link, useParams } from "react-router-dom"; 
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import classes from '../styles.module.css';
import CardActions from '@material-ui/core/CardActions';
import Grid from '@material-ui/core/Grid';

const CURRENT_POST = gql`
	  query Post($id: ID!) {
	  post(id: $id) {
	    title
	    body
	  }
	}
	`;

export default function Post() {
	let { post_id } = useParams();
	let next = '/posts/' + (parseInt(post_id) + 1);
	let back = '/posts/' + (parseInt(post_id) - 1);
	const {loading, error, data} = useQuery(CURRENT_POST, {
		variables: { id: post_id }
	});
	if (loading) return <> <PostLoad /> </>;
	if (error) return <p> error... </p>;

	return (
		<>
		<div className={classes.postContainer}>
			<Grid container >
			<h1> Post ID: { post_id } from: https://graphqlzero.almansi.me/api  </h1>
				<Grid item xs={12}>
					<Grid container justify="center" spacing="5">
					<Grid item> 
						<Card className={classes.postCardSingle}> 
							<CardContent> 
							<Typography className={classes.title} color="textSecondary" gutterBottom>
					        </Typography>
					        <Typography variant="h5" component="h2">
					          {data.post.title} 
					        </Typography>
					        <Typography className={classes.pos} color="textSecondary">
					        </Typography>
					        <Typography variant="body2" component="p">
					          {data.post.body}
					        </Typography>

						</CardContent>
						<CardActions>
				         <Link className={classes.link} to={back}> <Button variant="outlined" color="primary" size="large">  Previous Post </Button> </Link>
				         <Link className={classes.link} to={next}> <Button variant="outlined" color="primary" size="large">  Next </Button> </Link>
				      </CardActions>
					</Card> </Grid>
					</Grid>
				</Grid>
			</Grid>
		</div>
		</>
	)
}