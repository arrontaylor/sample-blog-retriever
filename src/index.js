import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { ApolloClient , ApolloProvider, InMemoryCache} from "@apollo/client";

const client = new ApolloClient({
	  cache: new InMemoryCache(),
  uri: "https://graphqlzero.almansi.me/api"
});

ReactDOM.render(
  <React.StrictMode>
  	<ApolloProvider client={client}>
    	<App client={client} />
    </ApolloProvider>
  </React.StrictMode>,
  document.getElementById('root')
);
